# Concourse Resource Type for Debian Security Announcements

This [Concourse CI](https://concourse-ci.org/) resource type checks the
[Debian Security Announcements](https://www.debian.org/security/) and reports the latest security announcements
this can be used to trigger registry-image or docker-image builds when security updates are released.

The source code for the resource type is available at https://git.dittberner.info/jan/concourse-dsa-resource

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Usage

To use the resource type in your own Concourse pipeline you will have to define the resource type and a resource:

```yaml
---
resource_types:
- name: debian-dsa
  type: registry-image
  source:
    repository: jandd/concourse-dsa-resource
    tag: latest

resources:
- name: debian-dsa-version
  type: debian-dsa
  source: {}

jobs:
- name: security-update
  plan:
  - get: debian-dsa-version
    trigger: true
```

The resource type supports the check and in operations.

## `check` operation

The check operations returns a list of versions consisting of an ISO date and the title of the available update. A
shortened and pretty-printed example output looks like this:
     
* Input

  ```json
  {}
  ```

* Output

  ```json
  [
    {
      "date": "2023-01-18",
      "dsa": "DSA-5322"
    },
    {
      "date": "2023-01-19",
      "dsa": "DSA-5323"
    }
  ]
  ```

## `in` operation

The in operation takes a version as described in Concourse's resource type implementation documentation and returns
metadata. If the version exists. The following input example returns the output below:

* Input

  ```json
  {
    "source": {},
    "params": {},
    "version": {
      "date": "2023-01-18",
      "dsa": "DSA-5322"
    }
  }
  ```

* Output

  ```json
  {
    "version": {
      "date": "2023-01-18",
      "dsa": "DSA-5322"
    },
    "metadata": {
      "link": "https://www.debian.org/security/2023/dsa-5322",
      "package": "firefox-esr",
      "description": "Multiple security issues have been found in the Mozilla Firefox web\nbrowser, which could potentially result in the execution of arbitrary\ncode, information disclosure or spoofing."
    }
  }
  ```