package dsa

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestResource_Check(t *testing.T) {
	input := strings.NewReader("{}")
	output := &bytes.Buffer{}

	resource := NewResource(input, output)

	err := resource.Check()

	assert.NoError(t, err)

	result := output.String()

	assert.NotEmpty(t, result)
}

func TestResource_Get(t *testing.T) {
	rdf, err := getRdfData(false)

	item := rdf.Items[0]

	parts := strings.SplitN(item.Title, " ", 2)

	require.NoError(t, err)

	input := strings.NewReader(fmt.Sprintf(
		`{"source":{},"params":{},"version":{"date":"%s","dsa":"%s"}}`, item.Date, parts[0],
	))
	output := &bytes.Buffer{}

	resource := NewResource(input, output)

	err = resource.Get("/tmp")

	assert.NoError(t, err)

	result := output.String()

	assert.NotEmpty(t, result)
}
