/*
concourse-dsa-resource a Concourse CI resource type to get Debian security update information

Copyright Jan Dittberner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package dsa

import (
	"fmt"
)

type rdfItem struct {
	Resource string `xml:"resource,attr"`
}

type rdfSeq struct {
	Items []rdfItem `xml:"http://www.w3.org/1999/02/22-rdf-syntax-ns# li"`
}

type rdfItems struct {
	RdfSeq rdfSeq `xml:"http://www.w3.org/1999/02/22-rdf-syntax-ns# Seq"`
}

type rssChannel struct {
	Title string   `xml:"http://purl.org/rss/1.0/ title"`
	Items rdfItems `xml:"http://purl.org/rss/1.0/ items"`
}

type rssItem struct {
	Title       string `xml:"http://purl.org/rss/1.0/ title"`
	Link        string `xml:"http://purl.org/rss/1.0/ link"`
	Description string `xml:"http://purl.org/rss/1.0/ description,omitempty"`
	Date        string `xml:"http://purl.org/dc/elements/1.1/ date"`
}

func (i rssItem) String() string {
	return fmt.Sprintf(
		"Item[Title: %s\nLink: %s\nDate: %s]",
		i.Title,
		i.Link,
		i.Date,
	)
}

type rdfData struct {
	Channel rssChannel `xml:"http://purl.org/rss/1.0/ channel"`
	Items   []rssItem  `xml:"http://purl.org/rss/1.0/ item"`
}
