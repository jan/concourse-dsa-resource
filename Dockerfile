FROM golang:1.19-bullseye AS builder

WORKDIR /build
COPY go.mod go.sum /build/
COPY cmd /build/cmd/
COPY internal /build/internal/

RUN go test -v ./...
RUN CGO_ENABLED=0 go build ./cmd/debian-dsa

FROM alpine:3

COPY --from=builder /build/debian-dsa /opt/resource/check
COPY README.md COPYING /
RUN ln /opt/resource/check /opt/resource/in
RUN ln /opt/resource/check /opt/resource/out