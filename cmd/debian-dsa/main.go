/*
concourse-debian-dsa a Concourse CI resource type to get Debian security update information

Copyright Jan Dittberner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"log"
	"os"
	"path"

	"git.dittberner.info/concourse-dsa-resource/internal/resource/dsa"
)

func main() {
	binaryName := path.Base(os.Args[0])

	resource := dsa.NewResource(os.Stdin, os.Stdout)

	var err error

	switch binaryName {
	case "in":
		if len(os.Args) != 2 {
			log.Fatal("invalid number of arguments, expected destination directory as only argument")
		}

		err = resource.Get(os.Args[1])
	case "out":
		err = dsa.ErrNotSupported
	default:
		err = resource.Check()
	}

	if err != nil {
		log.Fatalf("resource failed to handle operation: %v", err)
	}
}
