.PHONY: all docker-image

all: docker-image

docker-image: go.mod $(shell find -type f -name '*.go')
	docker build -t jandd/concourse-dsa-resource:latest .
